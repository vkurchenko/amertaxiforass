package test;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.By;
import page.SessionHelper;
import java.util.stream.Stream;

@DisplayName("Авторизация пользователя.")
public class AuthorizationWithIncorrectLogin extends TestBase {

    public SessionHelper sessionHelper;

    static Stream<Arguments> dataSource()
    {
        return Stream.of(
                Arguments.of( "CASE 1. Заполнено только поле 'пароль'",
                        "",
                        "2a0e7e93",
                        "Номер телефона и пароль должны быть заполнены."),
                Arguments.of( "CASE 2. Заполнено только поле 'номер'",
                        "+375295178679",
                        "",
                        "Номер телефона и пароль должны быть заполнены."),
                Arguments.of( "CASE 3. Заполнено поле 'номер' незарегистрированным номером.",
                        "+375291234578",
                        "2a0e7e93",
                        "Указанный номер телефона не зарегистрирован."),
                Arguments.of( "CASE 4. Заполнено поле 'пароль' неверными данными.",
                        "+375291234567",
                        "2a0e7e90",
                        "Вы ввели неверный пароль.")
        );
    }

    //https://vmmreg32.testrail.net/index.php?/cases/view/4289226
    //https://vmmreg32.testrail.net/index.php?/cases/view/4296167
    @DisplayName( "Негативные сценарии." )
    @ParameterizedTest(name = "{0}")
    @MethodSource("dataSource")
    public void firstTest(String name, String number, String password, String error){
        main.goTo();
        sessionHelper = new SessionHelper(driver);
        sessionHelper.login(number, password);
        basePage.checkElementDisplayed(By.xpath("//div[@class='popup-text']"));
        basePage.checkElementDisplayed(By.xpath("//div[contains(text(),\"" + error + "\")]"));
    }
}
