package test;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;

@DisplayName("Авторизация пользователя.")
public class AuthorizationWithAnExistingUser extends TestBase {

    //https://vmmreg32.testrail.net/index.php?/cases/view/4296153
    @Test
    @DisplayName("Authorization with an existing user")
    public void authorizationWithAnExistingUser(){
        main.goTo().login();
        basePage.checkElementDisplayed(By.xpath("//span[@id='orders_tab']"));
        main.logout();
    }
}