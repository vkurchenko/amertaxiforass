package test;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;

@DisplayName("Проверка перехода на страницу 'Контакты'")
public class CheckTransitToContacts extends TestBase {

    //https://vmmreg32.testrail.net/index.php?/cases/view/4289226
    @Test
    @DisplayName("Переход на страницу 'Контакты'")
    public void checkTransitToContacts(){
        main.goTo().login();
        basePage.click(By.xpath("//span[@id='contact_tab']"));
        contacts.checkPageIsCorrect();
        main.logout();
    }
}
