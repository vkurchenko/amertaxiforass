package test;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import page.BasePage;
import page.Main;
import page.Contacts;

import java.util.concurrent.TimeUnit;

public class TestBase {

    public WebDriver driver;
    public Main main;
    public Contacts contacts;
    public BasePage basePage;

    @BeforeEach
    public void start(){
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize(); //развернуть на все окно
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS); //ожидалка
        main = PageFactory.initElements(driver, Main.class);
        contacts = PageFactory.initElements(driver, Contacts.class);
        basePage = PageFactory.initElements(driver, BasePage.class);
    }

    @AfterEach
    public void finish(){
        driver.quit(); //закрыть браузер
    }
}
