package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SessionHelper extends HelperBase {

    public SessionHelper(WebDriver driver) {
        super(driver);
    }

    public void login(String number, String password) {
        type(By.id("login_phone_field"), number);
        type(By.id("login_pass_field"), password);
        driver.findElement(By.xpath("//button[@id='login_finish']")).click();
    }
}
