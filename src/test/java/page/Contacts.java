package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Contacts extends BasePage {

    public Contacts(WebDriver driver) {
        super(driver);
    }

    public void checkPageIsCorrect(){
        checkElementDisplayed(By.xpath("//span[text()='Контакты для связи']"));
//        return true;
    }
}
