package page;

import org.openqa.selenium.WebDriver;

public class Main extends BasePage {

    public SessionHelper sessionHelper;

    String SITE_URL = "https://americantaxi.andersenlab.com/americantaxi/login.php";
    public Main (WebDriver driver) {
        super(driver);
    }

    public Main goTo(){
        driver.get(SITE_URL);
        return this;
    }

    //Заголиниться
    public Main login() {
        sessionHelper = new SessionHelper(driver);
        sessionHelper.login("+375295178679", "2a0e7e93");
        return this;
    }
}
